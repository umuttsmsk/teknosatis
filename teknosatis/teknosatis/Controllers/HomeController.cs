﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using teknosatis.Models;
using teknosatis.DAL;
using PagedList;
using System.Globalization;

namespace teknosatis.Controllers
{
    public class HomeController : Controller
    {
        private teknosatisContext db = new teknosatisContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Arama(string arama)
        {
            var items = (from p in db.Uruns where (p.Ad.Contains(arama) || p.Detay.Contains(arama) || p.Kategori.Contains(arama)) orderby p.ID descending select p).ToPagedList(1, 4);
            ViewBag.aranan = arama;
            return View(items);
        }

        [Route("Home/Arama/{arama}/{sayfa}")]
        public ActionResult Arama(string arama, int sayfa)
        {
            var items = (from p in db.Uruns where (p.Ad.Contains(arama) || p.Detay.Contains(arama) || p.Kategori.Contains(arama)) orderby p.ID descending select p).ToPagedList(sayfa, 4);
            ViewBag.aranan = arama;
            return View(items);
        }

        [Route("Urunler/{Kategori}/{sayfa}")]
        public ActionResult Listele(string Kategori, int sayfa = 1)
        {
            ViewBag.Title = Kategori;
            ViewBag.Kategori = Kategori;
            var query = (from p in db.Uruns where (p.Kategori == Kategori) orderby p.ID descending select p).ToPagedList(sayfa, 4);
            return View(query);
        }

        public ActionResult UrunDetay(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            Urun urun;
            urun = db.Uruns.Find(id);
            db.SaveChanges();
            return View(urun);
        }
        public ActionResult ChangeCulture(string lang,string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public ActionResult index_goster()
        {
            teknosatisContext db = new teknosatisContext();
            List<Urun> urun6 = db.Uruns.OrderByDescending(i => i.ID).Take(6).ToList();
            return PartialView(urun6);
        }
    }
}