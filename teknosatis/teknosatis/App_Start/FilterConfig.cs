﻿using System.Web;
using System.Web.Mvc;
using teknosatis.Controllers;

namespace teknosatis
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
