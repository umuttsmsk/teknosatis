﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using teknosatis.DAL;
using teknosatis.Models;

namespace teknosatis
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            using (teknosatisContext db = new teknosatisContext())
            {
                int uyeAdet;
                uyeAdet = (from i in db.Uyes select i).Count();

                if (uyeAdet < 1)
                {
                    Uye uye = new Uye { Ad = "Admin", Soyad = "Admin", Eposta = "admin@admin.com", Sifre = "e10adc3949ba59abbe56e057f20f883e", Rutbe = "Yönetici" };

                    db.Uyes.Add(uye);
                    db.SaveChanges();
                }
            }
            
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session == null) return;
            var cultureInfo = (CultureInfo)Session["Culture"];
            if (cultureInfo == null)
            {
                var languageName = "tr";
                cultureInfo = new CultureInfo(languageName);
                Session["Culture"] = cultureInfo;
            }
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture =
            CultureInfo.CreateSpecificCulture(cultureInfo.Name);
        }
    }
}
