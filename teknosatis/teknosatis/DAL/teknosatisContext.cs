﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using teknosatis.Models;

namespace teknosatis.DAL
{
    public class teknosatisContext : DbContext
    {
        public teknosatisContext() : base("teknosatisContext") { }
        public DbSet<Uye> Uyes { get; set; }
        public DbSet<Urun> Uruns { get; set; }
        public DbSet<Iletisim> Iletisims { get; set; }
    }
}