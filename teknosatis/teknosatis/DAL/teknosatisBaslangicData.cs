﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using teknosatis.Models;

namespace teknosatis.DAL
{
    public class teknosatisBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<teknosatisContext>
    {
        protected override void Seed(teknosatisContext context)
        {
            #region Ürünler

            var Urunler = new List<Urun>
            {
                

            };
            Urunler.ForEach(s => context.Uruns.Add(s));
            context.SaveChanges();
            
            #endregion


            #region Uyeler

            var Uyeler = new List<Uye>
            {
            };

            Uyeler.ForEach(s => context.Uyes.Add(s));
            context.SaveChanges();

            #endregion
        }
    }
}