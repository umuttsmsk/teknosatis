﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(teknosatis.Startup))]
namespace teknosatis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
